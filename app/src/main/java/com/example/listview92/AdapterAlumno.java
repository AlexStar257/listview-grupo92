package com.example.listview92;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class AdapterAlumno extends ArrayAdapter<AlumnoItem> implements Filterable {
    private int groupId;
    private Activity context;
    private List<AlumnoItem> originalList;
    private List<AlumnoItem> filteredList;
    private LayoutInflater inflater;
    private ItemFilter itemFilter;

    public AdapterAlumno(Activity context, int groupId, int id, List<AlumnoItem> list) {
        super(context, id, list);
        this.context = context;
        this.groupId = groupId;
        this.originalList = new ArrayList<>(list);
        this.filteredList = new ArrayList<>(list);
        inflater = context.getLayoutInflater();
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View itemView = inflater.inflate(groupId, parent, false);
        ImageView imagenAlumno = itemView.findViewById(R.id.imgAlumno);
        imagenAlumno.setImageResource(filteredList.get(position).getImageId());
        TextView textNombre = itemView.findViewById(R.id.lblNombre);
        textNombre.setText(filteredList.get(position).getTextNombre());
        TextView textMatricula = itemView.findViewById(R.id.lblMatricula);
        textMatricula.setText(filteredList.get(position).getTextMatricula());
        return itemView;
    }

    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getView(position, convertView, parent);
    }

    @Override
    public int getCount() {
        return filteredList.size();
    }

    @Override
    public AlumnoItem getItem(int position) {
        return filteredList.get(position);
    }

    @Override
    public Filter getFilter() {
        if (itemFilter == null) {
            itemFilter = new ItemFilter();
        }
        return itemFilter;
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            String filterString = constraint.toString().toLowerCase();

            if (filterString.isEmpty()) {
                results.count = originalList.size();
                results.values = originalList;
            } else {
                ArrayList<AlumnoItem> filteredItems = new ArrayList<>();

                for (AlumnoItem item : originalList) {
                    if (item.getTextNombre().toLowerCase().contains(filterString)) {
                        filteredItems.add(item);
                    }
                }

                results.count = filteredItems.size();
                results.values = filteredItems;
            }

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredList = (List<AlumnoItem>) results.values;
            notifyDataSetChanged();
        }
    }
}
